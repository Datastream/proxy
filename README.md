# Datastream Proxies

## Objectives

Send HTTP request discreetly to a server, and cares of the latency and response time.

## Start

``bundle exec rackup config.ru``

## Configuration

- PROXY_MIN_WAIT        (Integer, in seconds, default 1). Sleep at least N seconds before send a new request on a host
- PROXY_RANDOM_WAIT_USE (Boolean, "true/false", default false). By default, the algo use PROXY_MIN_WAIT
- PROXY_MIN_RANDOM_WAIT (Integer, in second, default 1)
- PROXY_MAX_RANDOM_WAIT (Integer, in second, default 3)
- PROXY_MULTI_WAIT      (Integer, default 1). It multiply the delay (req_end - req_begin * VAL)
- TH_CHANGE_AGENT       (Integer, default 100). % of chance to change the user_agent and reset the cookies.

## Routes

- ``/usable`` list all domains
- ``/usable/reset/:host`` reset the stats of the host
- ``/`` reuest (require a ``url`` parameter)

## Request Algorithm

Each request is sorted by domain name.
For a request to a domain name, a lock is created and released at the end of the request.
The request wait a certain amount of time before being sent.
When the request is sent, the response time + latency is registrated.

The time to wait before sending a request to a given domain name is:
- Calculated since the last time the proxy sent a request (if the last request was 1 week ago, there is no waiting time for the next reuest).
- If the proxy is configured to be random: a random number of seconds between [MIN, MAX], else it is a fixed value.
  - This waiting duration is multiplied by a coeficient (configured)
  - An additionnal waiting time is added, equal to the duration of the request latency.
