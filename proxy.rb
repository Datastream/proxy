require 'rubygems'
require 'sinatra'
require 'pry' if ENV["DEBUG"] = "true"

require 'sinatra_auth_token'

SinatraAuthToken.init_auth_token!
helpers SinatraAuthToken::SinatraHelper

require_relative 'proxy/client'
$client = Proxy::Client.new

require_relative 'proxy/routes'
