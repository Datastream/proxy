require "test/unit"
require 'open-uri'
require 'uri'
require 'http'
require 'json'
require 'mechanize'

class TestGameReal < Test::Unit::TestCase

  def test_basic
    http = fork do
        exec("rake hello > /dev/null")
    end
    proxy = fork do
        exec("PROXY_MIN_WAIT=0 PROXY_MULTI_WAIT=0 PROXY_MIN_WAIT_GLOBAL=0 bundle exec rackup config.ru -p 10900 2> /dev/null")
    end
    sleep 3
    begin
      t1 = Time.new
      1000.times do
        p = Mechanize.new.get("http://localhost:10900/?url=aHR0cDovL2xvY2FsaG9zdDoxMDkwMQo=")
        assert p.title == "hello world"
      end
      t2 = Time.now
      puts "Result : #{(t2 - t1) / 10.0} ms by request"
    ensure
      # http.kill
      # proxy.kill
      Process.kill "TERM", http
      Process.waitpid http
      Process.kill "TERM", proxy
      Process.waitpid proxy
    end
  end

end
