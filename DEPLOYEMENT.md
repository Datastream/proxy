# Notes
none

# Requirements

- Ruby 2.3 or higher
- RubyGem
- ruby-dev
- zlib1g-dev

# Installation

    bundle install

# Configuration
## Required
nope

## Optional
see the readme

# Usage
## Start

    bundle exec rackup config.ru -p 3001

