get '/usable' do
  protect! params

  return $client.next_usage.to_json
end
