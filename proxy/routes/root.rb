# coding: utf-8
# Decode the URL
# Get the lock on the mutex
# Set the time when the mutex will be usable again
get '/' do
  protect! params

  status 404; return "404 not found" if params[:url].nil? # no url = fake 404
  url = Base64.strict_decode64(params[:url]).strip
  host = URI.parse(url).host
  $client.mutex[host] ||= Mutex.new
  wait = Helper.get_wait_duration

  begin
    Helper.wait_for_lock host
    Helper.wait_for host
    Helper.random_user_agent
    _, duration, _, ret = Helper.duration_for do
      begin
        page = $client.agent.get(url)
        page.content
      rescue => err
        status err.response_code.to_i
        err.message
      end
    end.flatten

    sleep_duration_final = Helper.get_sleep_duration_final(duration, wait)
    $client.next_usage[host] = {time: Time.now.to_i + sleep_duration_final, duration: sleep_duration_final}

    STDERR.puts "Request duration of\t'#{host}'\t=>\t#{duration.round(3)} s"
    STDERR.puts "Next usage of      \t'#{host}'\t=>\t'#{Time.at $client.next_usage[host][:time]}' \t(#{sleep_duration_final} seconds)"

    return ret # end
  rescue => err
    STDERR.puts err, err.backtrace
    raise
  ensure
    $client.mutex[host].unlock
  end
end
