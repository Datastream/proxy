get '/usable/reset/:host' do
  protect! params

  host = params[:host]
  $client.next_usage[host] = nil
  return $client.next_usage.to_json
end
