TH_CHANGE = (ENV["TH_CHANGE_AGENT"] || 100).to_i

module Helper

  extend self

  def get_wait_duration
    if ENV["PROXY_RAND_WAIT_USE"] == "true"
      min = (ENV["PROXY_RAND_MIN_WAIT"] || 1).to_i
      max = (ENV["PROXY_RAND_MAX_WAIT"] || 3).to_i
      rand(min..max)
    elsif ENV["PROXY_MIN_WAIT"]
      ENV["PROXY_MIN_WAIT"].to_i
    else
      1
    end
  end

  def wait_for(host)
    return if $client.next_usage[host].nil?
    t = $client.next_usage[host][:time].to_i
    if t && t > Time.now.to_i
      duration = t - Time.now.to_i
      #puts "Waits #{duration} seconds"
      sleep(duration)
    end
  end

  def random_user_agent
    $client.agent = Mechanize.new.random_user_agent if rand(1..TH_CHANGE) == 1
  end

  def self.get_sleep_duration_final(duration, wait)
    sleep_duration = (duration * (ENV["PROXY_MULTI_WAIT"] || 1).to_i).to_i + wait
    [[sleep_duration, (ENV["PROXY_MIN_WAIT_GLOBAL"] || 1).to_i].max, (ENV["PROXY_MAX_WAIT_GLOBAL"] || 5).to_i].min
  end

  def duration_for
    tStart = Time.now
    result = yield
    tEnd = Time.now
    {duration: tEnd - tStart, result: result}
  end

  def wait_for_lock(host)
    while $client.mutex[host].try_lock == false # while cannot aquire the lock, sleep
      wait_for host
    end # the thread has the lock
  end

end
