require 'json'
require 'base64'
require 'mechanize'
require 'mechanize-random-agent'

require 'pry' if ENV["DEBUG"] = "true"

require_relative 'helpers/main'

require_relative 'routes/usable'
require_relative 'routes/usable/reset/:host'
require_relative 'routes/root'
