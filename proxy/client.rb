require 'mechanize'
require 'mechanize-random-agent'

module Proxy
  class Client

    attr_accessor :next_usage, :mutex, :agent
    def initialize
      @next_usage = {}
      @mutex = {}
      @agent = Mechanize.new.random_user_agent
    end

  end
end
